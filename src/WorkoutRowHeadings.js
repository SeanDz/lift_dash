import React from "react";

// methods/variables


export const WorkoutRowHeadings = (props) => (
    // implicit braces & return

    <React.Fragment>
        <th>{/* Blank Cell */}#</th>

        {/* Note: # of sets is always highest on lift 1. Therefore it sets the row header length */}
        { props.workout_data.lift_data[0].set_data.map((lift_data, i) => {
            return (
                <tr>
                    <td>
                        Set {i}
                    </td>
                </tr>
            )
        }) }

    </React.Fragment>
);


