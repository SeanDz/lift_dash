import React from "react";
import {css} from "emotion";

// methods/variables


export const StatusBar = (props) => (
    // implicit braces & return

    <div className={base_status_bar_style}>
        <p className={status_bar_element_style}>{props.message}</p>
    </div>
);


const base_status_bar_style = css`
  grid-area: status_bar;
  height: 3vh;
  width: 100%;
  background-color: lightgoldenrodyellow;
  color: saddlebrown;
  font-size: 1.2em;
  padding: 5vh 2vw;
  margin-bottom: 3vh;
  border: 3px solid burlywood;
`;

const status_bar_element_style = css`
  margin-top: -3vh;
  font-style: italic;
`;