import React from "react";
import {css} from "emotion";
import Lodash from "lodash";

// methods/variables




// todo TABLE HEADERS ========================================================


const render_exercise_names = (props) => {
    return (
        <tr>
            <td></td>
            {
            props.today_workout.lift_data.map((array_item, index) => (
                <td colSpan={3} key={index}>{array_item.lift_name}</td>
                ))}
        </tr>
    )
};

// const push_exercises_into_array = (props) => {
//
//     let exercise_name_index;
//     let exercise_headers = [];
//
//         if (props.today_workout) {
//             for (exercise_name_index = 0; exercise_name_index < props.today_workout.lift_data.length; exercise_name_index ++) {
//                 exercise_headers.push(
//                     <td key={exercise_name_index}>
//                         { props.today_workout.lift_data[exercise_name_index].lift_name }
//                     </td>
//                 )
//             }
//         console.log('TodayTable.js:34, exercise headers', exercise_headers);
//         return <tr><td>Hi</td></tr>
//     }
// };







// TABLE ROW JSX =========================================================

let td_block_index;
let row_index;

const render_lift_tds = (props, row_index, td_block_index) => {

    if (Lodash.isEmpty(props.today_workout) === false) {

        let weight_property = props.today_workout.lift_data[td_block_index].set_data[row_index].weight;
        let reps_property = props.today_workout.lift_data[td_block_index].set_data[row_index].reps;
        let reserve_property = props.today_workout.lift_data[td_block_index].set_data[row_index].reserve;

        return (
            <React.Fragment key={td_block_index}>

                {/*//add some logs to see what I'm getting*/}
                {console.log ('TodayTable:57. passage of row_index', row_index)}
                {console.log ('TodayTable:58. passage of td_block_index', td_block_index)}

                <td><input type="text" className={input_style} defaultValue=
                    { weight_property ?
                      weight_property : "not found"
                    }
                /></td>
                <td><input type="text" className={input_style} defaultValue={reps_property ? reps_property : "not found"}/></td>
                <td><input type="text" className={input_style} defaultValue={reserve_property != null ? reserve_property : "not found"}/></td>
            </React.Fragment>
        )
    }
};

// ** FOR loops must be outside returns
// Arrays flatten automatically
const render_row = (props, row_index) => {
    let lift_td_block_jsx = [];

    if (props.today_workout.lift_data) {

        for (td_block_index = 0; td_block_index < props.today_workout.lift_data.length; td_block_index ++) {
            lift_td_block_jsx.push(render_lift_tds(props, row_index, td_block_index));
        }
    }

    return (
    <tr className={row_style} key={row_index}>
        <td className={small_headers}>Set {row_index + 1}</td>
        {lift_td_block_jsx}
    </tr>
    )
};

const render_all_rows = (props) => {
    let row_jsx = [];

    if(props.today_workout) {

        for (row_index = 0; row_index < props.today_workout.lift_data[0].set_data.length; row_index ++) {
        row_jsx.push(render_row(props, row_index));
    }
        return (
            <React.Fragment>
                {row_jsx}
            </React.Fragment>
        )
    }

};



// RENDER ======================================================

export const TodayTable = (props) => {


    return (
        // implicit braces & return

        <React.Fragment>

            <form action="">

                <div className={ table_wrapper_style }>

                    <table className={ table_style }>
                        <tbody>
                        <tr>
                            <th colSpan={ 13 }>
                                <h3 className={ cell_style }>Today: Workout { props.today_workout.program_day.toUpperCase() } </h3>
                            </th>
                        </tr>

                        { render_exercise_names(props) }


                        <tr>
                            <td className={ cell_style }></td>


                            { /* DISPLAY SUBHEADS ============================ */ }

                            { /* For the real data source: return an array as a const.
                             Run .map method on the array
                             Use a length property variable assignment and then a loop if needed to create a false array*/ }
                            { props.today_workout.lift_data.map((array_value, i) => (
                                    <React.Fragment key={ i }>
                                        <td className={ css`${cell_style} ${small_headers}; background-color : "green"` }>Weight</td>
                                        <td className={ css`${cell_style} ${small_headers}` }>Reps</td>
                                        <td className={ css`${cell_style} ${small_headers}` }>Reserve</td>
                                    </React.Fragment>
                                ))}

                            { /* ============================================= */ }

                        </tr>

                        { render_all_rows(props) }


                        <tr>
                            <td colSpan={ 13 }>
                                <textarea name="workout_notes" id="note_textarea" cols="30" rows="5"
                                          className={ textarea_field_style } placeholder='Enter notes here'>{props.today_workout.workout_notes}</textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div className={button_group_style}>
                        <button type='button' className={ save_button_style }>Save Incomplete</button>
                        <button type='button' className={ css`${save_button_style}; background-color: #305aff` }>Save Completed
                        </button>
                    </div>
                </div>
            </form>
        </React.Fragment>
    );
}

const save_button_style = css`
  font: italic 1.5rem "Open Sanss", sans-serif;
  color : whitesmoke;
  padding: 1vw 3vw;
  background-color: #4088ff;
  border-radius: 10%;
  //width: 50%;
  &:hover {
    background-color: aqua;
  }
`;

const button_group_style = css`
  display: flex;
  justify-content: space-around;
`;

// const sync_button_style = css`
//   color : whitesmoke;
//   background-color: #127d26;
//   border-radius: 10%;
//   width: 100%;
// `;


const table_style = css`
  color: #fefefe;
  text-align: center;
  border-collapse: collapse;
`;

// const table_container_style = css`
//   background-color: brown;
//   padding: 2vw 20px;
// `;


const table_wrapper_style = css`
  border-radius: 3%;
  background-color: #1a785c;
  padding: .7vw;
  margin: .5vw;
  max-width: 500px;
  border: 3px solid #003300;
`;

const cell_style = css`
  //border : 1px dashed blue;
  //width : 1vw;
`;

const input_style = css`
  width: 100%;
  text-align: center;
  background-color: black;
  font-size: 1.3rem;
`;


const textarea_field_style = css`
    background-color : black;
    color : whitesmoke;
    width : 100%;
    font-style: italic;
`;

const row_style = css`
  margin-top: 0px;
  padding-top: 0px;
  margin-bottom: 0px;
  padding-bottom: 0px;
`;


/// Modifiers ==============================

const small_headers = css`
  font-size: 1rem;
`;