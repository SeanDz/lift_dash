import React from "react";
import {css} from "emotion";

// methods/variables


export const HistoryTable = (props) => (
    // implicit braces & return

    <div className={outermost_div_style}>

        {/* Table Title */}

        <table style={{width : '100%'}}>
            <tbody>
                <tr><td colSpan={13}><h5>July 11, 2018 : Workout F</h5></td></tr>
                <tr>
                    <td></td>
                    <td colSpan={3}>TB OH Press</td>
                    <td colSpan={3}>Pullups</td>
                    <td colSpan={3}>Dips</td>
                    <td colSpan={3}>Lat Raises</td>
                </tr>

                <tr>
                    <td></td>
                {[1, 1, 1, 1].map((x, i) => {
                    return (
                        <React.Fragment key={i}>
                            <td className={small_headers}>Weight</td>
                            <td className={small_headers}>Reps</td>
                            <td className={small_headers}>Reserve</td>
                        </React.Fragment>
                    )
                })}
                </tr>

                <tr>
                    <td className={css `${small_headers} ${table_data_style}`}>Set 1</td>
                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>

                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>

                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>

                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>
                    <td className={table_data_style}>100</td>

                </tr>

                <tr><td colSpan={13}><textarea className={textarea_style} name="" id="" cols="30" rows="8" value="Notes from prior workout. Notes from prior workout. Notes from prior workout. Notes from prior workout. Notes from prior workout. Notes from prior workout. Notes from prior workout." readOnly></textarea></td></tr>

            </tbody>
        </table>

        {/* Exercise Headings */}
        {/* Set Rows */}
        {/*  */}


    </div>
);

// CSS ====================================================================

const outermost_div_style = css`
  background-color: #0d2878;
  color: aliceblue;
  max-width: 500px;
  //height: 200px;
  border-radius: 3%;
  text-align: center;
  padding: 1vw;
  font-size: .9em;
  border: 2px solid darkblue;
`;

const small_headers = css`
  font-size: .8em;
`;

const textarea_style = css`
  background-color: #0d2878;
  color: aliceblue;
  width: 100%;
  font-style: italic;
`;

const table_data_style = css`
  border-collapse: collapse;
  border: 1px dashed burlywood;
`;