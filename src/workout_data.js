export const workout_data = {
    workout_heading : 'Workout A: Stool, Rows, Incline, Lat Raises',
    start_date : 'July 10, 2018',
    workout_status : 'incomplete',
    workout_notes : "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam deleniti enim fuga id reiciendis. Ab at delectus doloribus eligendi est harum id ipsam maiores, maxime molestias nemo odio pariatur tenetur.",
    lift_data : [
        {
            lift_name : "TB Stool",
            set_data : [
                {
                    weight  : 48.5,
                    reps    : 15,
                    reserve : null,
                },
                 {
                    weight  : 48.5,
                    reps    : 15,
                    reserve : null,
                },
                 {
                    weight  : 48.5,
                    reps    : 15,
                    reserve : null,
                }
            ],
        },
        {
            lift_name : "TB Rows",
            set_data  : [
                {
                    weight  : 56,
                    reps    : 15,
                    reserve : null,
                },
                {
                    weight  : 48.5,
                    reps    : 15,
                    reserve : 2,
                },
                {
                    weight  : 45,
                    reps    : 13,
                    reserve : 1,
                },
            ]
        },
        {
            lift_name : "DB Incline",
            set_data : [
                {
                    weight  : 8.5,
                    reps    : 15,
                    reserve : null,
                },
                {
                    weight  : 8.5,
                    reps    : 15,
                    reserve : 2,
                },
                {
                    weight  : 8.5,
                    reps    : 13,
                    reserve : 1,
                },
            ]
        },
        {
            lift_name : "Lat Raises",
            set_data : [
                {
                    weight  : 3.25,
                    reps    : 15,
                    reserve : null,
                },
                {
                    weight  : 3.25,
                    reps    : 15,
                    reserve : 1,
                },
                {
                    weight  : 3.25,
                    reps    : 15,
                    reserve : 1,
                }
            ],
        },

    ]
};


export const exercise_schedules = {
    a : [
        {
            name: "TB Stool",
            sets : 3
        },
        {
            name: "TB Rows",
            sets : 3
        },
        {
            name: "DB Incline",
            sets : 3
        },
        {
            name: "Lat Raises",
            sets : 3
        },
    ],
    b : [
        {
            name: "TB Stool",
            sets : 3
        },
        {
            name: "TB Rows",
            sets : 3
        },
        {
            name: "Pullups",
            sets : 3
        },
        {
            name: "Ab Wheel",
            sets : 3
        },
    ],
    c : [
        {
            name: "TB OHP",
            sets : 3
        },
        {
            name: "Pullups",
            sets : 3
        },
        {
            name: "Dips",
            sets : 3
        },
        {
            name: "Ab Wheel",
            sets : 3
        },
    ],
    d : [
        {
            name: "TB Stool",
            sets : 3
        },
        {
            name: "Pullups",
            sets : 3
        },
        {
            name: "DB Incline",
            sets : 3
        },
        {
            name: "Lat Raises",
            sets : 3
        },
    ],
    e : [
        {
            name: "TB Stool",
            sets : 3
        },
        {
            name: "TB Rows",
            sets : 3
        },
        {
            name: "Pullups",
            sets : 3
        },
        {
            name: "Ab Wheel",
            sets : 3
        },
    ],
    f : [
        {
            name: "TB OHP",
            sets : 3
        },
        {
            name: "TB Rows",
            sets : 3
        },
        {
            name: "Dips",
            sets : 3
        },
        {
            name: "Lat Raises",
            sets : 3
        },
    ],
};

export const program_data = {
    code : {
        a : 1,
        b : 2,
        c : 3,
        d : 4,
        e : 5,
        f : 6,
    },

    exercise_schedules : {
        a : {
            lift_1_name : 'TB Stool',
            lift_1_sets : 3,
            lift_2_name : 'TB Rows',
            lift_2_sets : 3,
            lift_3_name : 'DB Incline',
            lift_3_sets : 3,
            lift_4_name : 'Lat Raises',
            lift_4_sets : 3
        },
        b : {
            lift_1_name : 'TB Stool',
            lift_1_sets : 3,
            lift_2_name : 'TB Rows',
            lift_2_sets : 3,
            lift_3_name : 'Pullups',
            lift_3_sets : 3,
            lift_4_name : 'Ab Wheel',
            lift_4_sets : 3
        },
        c : {
            lift_1_name : 'TB OH Press',
            lift_1_sets : 3,
            lift_2_name : 'Pullups',
            lift_2_sets : 3,
            lift_3_name : 'Dips',
            lift_3_sets : 3,
            lift_4_name : 'Ab Wheel',
            lift_4_sets : 3
        },
        d : {
            lift_1_name : 'TB Stool',
            lift_1_sets : 3,
            lift_2_name : 'Pullups',
            lift_2_sets : 3,
            lift_3_name : 'DB Incline',
            lift_3_sets : 3,
            lift_4_name : 'Lat Raises',
            lift_4_sets : 3
        },
        e : {
            lift_1_name : 'TB Stool',
            lift_1_sets : 3,
            lift_2_name : 'TB Rows',
            lift_2_sets : 3,
            lift_3_name : 'Pullups',
            lift_3_sets : 3,
            lift_4_name : 'Ab Wheel',
            lift_4_sets : 3
        },
        f : {
            lift_1_name : 'TB OH Press',
            lift_1_sets : 3,
            lift_2_name : 'TB Rows',
            lift_2_sets : 3,
            lift_3_name : 'Dips',
            lift_3_sets : 3,
            lift_4_name : 'Lat Raises',
            lift_4_sets : 3
        },
    }

};

program_data.code//?