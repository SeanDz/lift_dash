import React, { Component } from 'react';
import {css} from 'emotion';
import Lodash from "lodash";
import {workout_data, program_data} from "./workout_data";
import "./App.css";

import {TodayTable} from "./components/TodayTable";
import {HistoryTable}          from "./components/HistoryTable";
import {StatusBar} from "./components/StatusBar";




// Database Connection ===============================================

const firebase = require("firebase");
require("firebase/firestore");

const config = {
    apiKey: "AIzaSyApQTC4Bc97cZ3jcQ-9uKEneY7Lh_5Nu2o",
    authDomain: "lift-dash.firebaseapp.com",
    databaseURL: "https://lift-dash.firebaseio.com",
    projectId: "lift-dash",
    storageBucket: "lift-dash.appspot.com",
    messagingSenderId: "355485418256"
};
firebase.initializeApp(config);
const db = firebase.firestore();


const firestore_settings = {
    timestampsInSnapshots: true
};
firebase.firestore().settings(firestore_settings);




// App Component ======================================================

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            box : '',
            status_bar_state : 'syncing',
            today_workout : {},
            workouts : []
        };
        console.log('App.js:44 this.state.today_workout', this.state.today_workout)
    }



    // check if the latest workout is completed. If yes, create a new workout. Otherwise, load the old one
        // data: on new workout creation, state variable today's workout is assigned. On usage of old workout, workout[0] is spliced out and assigned
        // TodayTable receives the new workout state variable. History tables are mapped into existence by the length of array old_workout
        // a nested map is performed on the set array length

    assign_new_workout_if_previous_completed = () => {

        // if there's no today workout yet, assign a new one
        if (Lodash.isEmpty(this.state.today_workout)) {
            console.log('App.js:54, today workout unassigned at this point, about to be assigned next');
            const last_workout = this.state.workouts[0].program_day;
            let workout_key = program_data.code[last_workout];
            let next_day_adjustment = workout_key + 1;
            if (next_day_adjustment === 7) {
                next_day_adjustment = 1;
            }
            // find the key based on the value
            let new_program_key = Object.keys(program_data.code)
                                        .find(key => program_data.code[key] === next_day_adjustment);

            this.setState((previous_state, props) => (
                {
                    today_workout : {
                        name : `Workout ${new_program_key.toString().toUpperCase()}`,
                        lifts : [
                            {
                                lift_name : program_data.exercise_schedules[new_program_key].lift_1_name,
                                number_of_sets : program_data.exercise_schedules[new_program_key].lift_1_sets
                            },
                            {
                                lift_name : program_data.exercise_schedules[new_program_key].lift_2_name,
                                number_of_sets : program_data.exercise_schedules[new_program_key].lift_2_sets
                            },
                            {
                                lift_name : program_data.exercise_schedules[new_program_key].lift_3_name,
                                number_of_sets : program_data.exercise_schedules[new_program_key].lift_3_sets
                            },
                            {
                                lift_name : program_data.exercise_schedules[new_program_key].lift_4_name,
                                number_of_sets : program_data.exercise_schedules[new_program_key].lift_4_sets
                            },
                        ]
                    }
                }
            ));
            console.log('App.js:90, this.state.today_workout', this.state.today_workout);
        }

    };






    handle_save = () => {
        db.collection("workout_history").doc('20180710').get()
            .then(docRef => {
                // console.log('docRef: ', docRef.data());
                this.setState({
                    box : docRef.data()
                });
            })
            .catch(error => { console.log('error: ', error)})
    };




    // ========================================================
    // 1. load the last 20 workouts into state
    // 2. fire the new workout assignment function IF the last one is already completed

    componentDidMount () {

        // put last 20 workouts into this.state.workouts. Then, re-assign incomplete
        // to state.today_workout
        db.collection('workout_history').orderBy('create_timestamp', 'desc').limit(20).get()
            .then(documents => {
                console.log('App.js:128, documents freshly grabbed', documents);

                documents.forEach(document => {
                    this.state.workouts.push(document.data())
                });

                // if there's an incomplete one, load that into a different state variable
                if (this.state.workouts[0].workout_status === 'incomplete') {
                    console.log('App.js:136, document[0], registered as status incomplete', console.log(this.state.workouts[0].workout_status));
                    this.setState((previous_state, props) => ({
                            today_workout : this.state.workouts[0],
                            workouts : this.state.workouts.filter((record) => (
                                record.workout_status !== 'incomplete'
                            ))
                        }
                    ));
                    console.log ('App.js:144, fresh setState. state today_workout.lift_data.length', this.state.today_workout.lift_data.length)
                } else {
                    this.assign_new_workout_if_previous_completed()
                }
            })
            .catch(error => console.log("Error:", error))
        ;
    }









    render() {

        return (
            <div className={top_level_style}>
                { /*<Container style={top_level_style}>*/ }


                { Lodash.isEmpty(this.state.today_workout) === true && <StatusBar message="Syncing, please wait until the exercise data fully loads..."/> }

                <div className={grid_style}>


                    { Lodash.isEmpty(this.state.today_workout) === false ?
                      <TodayTable handle_save={this.handle_save} today_workout={this.state.today_workout} /> :
                      <i className='fa fa-spinner fa-spin fa-5x' style={{
                          color: 'white'}}></i> }


                    <HistoryTable workout_data={workout_data} />
                    <HistoryTable workout_data={workout_data} />
                    <HistoryTable workout_data={workout_data} />

                </div>


                {/*</Container>*/}
            </div>
        );
    }
}


export default App;


// CSS ============================================================

const top_level_style = css`
  background-color: #222;  
  display: grid;
  grid-template-areas: 
    "status_bar status_bar"
    "tables tables";
`;

const grid_style = css`
  display: grid;
  grid-area: tables;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: .5vw;
  grid-row-gap: 1.5vh;
`;
